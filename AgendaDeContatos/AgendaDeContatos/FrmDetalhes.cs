﻿using AgendaDeContatos.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgendaDeContatos
{
    public partial class FrmDetalhes : Form
    {
        private Contato contato;

        public Contato ContatoCriado => FechouAoGravar ? contato : null;

        private bool FechouAoGravar = false;

        public FrmDetalhes()
        {
            InitializeComponent();
            pbFoto.Location = new Point(
                (panel1.Width / 2) - (pbFoto.Width / 2),
                pbFoto.Location.Y);

            contato = new Contato();
            Text = "Novo Contato";
        }

        public FrmDetalhes(Contato c) : this()
        {
            contato = c;
            Text = c.Nome;
        }       

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void FrmDetalhes_Load(object sender, EventArgs e)
        {
            txtNome.Text = contato.Nome;
            txtCelular.Text = contato.Celular?.Numero ?? "";
            txtTelefone.Text = contato.Telefone?.Numero ?? "";
            txtEmail.Text = contato.Email?.Endereco ?? "";
        }

        private void button_WOC1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNome.Text))
            {
                MessageBox.Show("Preencha o nome do contato");
                return;
            }
            contato.Nome = txtNome.Text;

            var cel = Celular.Criar(txtCelular.Text);
            if (cel == null)
            {
                MessageBox.Show("Celular Inválido!");
                return;
            }
            contato.Celular = cel;

            var tel = Telefone.Criar(txtTelefone.Text);
            if (tel == null)
            {
                MessageBox.Show("Telefone Inválido!");
                return;
            }
            contato.Telefone = tel;

            var mail = Email.Criar(txtEmail.Text);
            if (mail == null)
            {
                MessageBox.Show("Email Inválido!");
                return;
            }
            contato.Email = mail;
            FechouAoGravar = true;
            Close();
        }

        private void FrmDetalhes_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
