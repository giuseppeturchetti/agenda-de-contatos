﻿using AgendaDeContatos.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgendaDeContatos
{
    public partial class Form1 : Form
    {
        private List<Contato> listaDeContatos;


        public Form1()
        { 
            InitializeComponent();
        }         

        private void SetDataSource(IEnumerable<Contato> contatos)
        {
            contatos = contatos.OrderBy(c => c.Nome);
            bsContatos.DataSource = contatos;
        }

        private void ChecaTelefone(string numeroTelefone)
        {
            var telefone = Telefone.Criar(numeroTelefone);
            if (telefone == null)
            {
                MessageBox.Show("Telefone inválido!");
            }
            else
            {
                MessageBox.Show(telefone.Numero);
            }
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button_WOC1_Click(object sender, EventArgs e)
        {
            using (var f = new FrmDetalhes())
            {
                f.ShowDialog();
                if (f.ContatoCriado != null)
                {
                    listaDeContatos.Add(f.ContatoCriado);
                    SetDataSource(listaDeContatos);
                    textBox1.Text = "";
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var contato = bsContatos.Current as Contato;
            if (contato != null)
            {
                using (var f = new FrmDetalhes(contato))
                {
                    f.ShowDialog();
                    bsContatos.ResetCurrentItem();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists("contatos.txt"))
            {
                var arquivo = File.ReadAllText("contatos.txt");
                listaDeContatos =
                    JsonConvert.DeserializeObject<List<Contato>>(
                        arquivo, new JsonSerializerSettings()
                        {
                            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor
                        });
                SetDataSource(listaDeContatos);
            }
            else
            {
                listaDeContatos = new List<Contato>();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            var arquivo = JsonConvert.SerializeObject(listaDeContatos);
            File.WriteAllText("contatos.txt", arquivo);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SetDataSource(
                listaDeContatos.Where(c =>
                    c.Nome.ToLower().Contains(textBox1.Text.ToLower()))
            );
        }
    }
}
