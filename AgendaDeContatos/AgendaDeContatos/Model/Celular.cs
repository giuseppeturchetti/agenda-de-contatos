﻿using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace AgendaDeContatos.Model
{
    public class Celular
    {
        public string Numero { get; }

        [JsonConstructor]
        private Celular(string numero)
        {
            Numero = numero;
        }

        public static Celular Criar(string numero)
        {
            if (string.IsNullOrWhiteSpace(numero))
            {
                return new Celular("");
            }
            if (Regex.IsMatch(numero, @"^\d{2}9\d{8}$"))
            {
                return new Celular(numero);
            }
            return null;
        }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(Numero))
            {
                return Numero;
            }
            var sb = new StringBuilder();
            sb.Append("(");
            sb.Append(Numero[0]);
            sb.Append(Numero[1]);
            sb.Append(")");
            sb.Append(" ");
            sb.Append(Numero[2]);
            sb.Append(Numero[3]);
            sb.Append(Numero[4]);
            sb.Append(Numero[5]);
            sb.Append(Numero[6]);
            sb.Append("-");
            sb.Append(Numero[7]);
            sb.Append(Numero[8]);
            sb.Append(Numero[9]);
            sb.Append(Numero[10]);
            return sb.ToString();
        }
    }
}