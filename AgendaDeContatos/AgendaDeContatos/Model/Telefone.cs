﻿using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace AgendaDeContatos.Model
{
    public class Telefone
    {
        public string Numero { get; }

        [JsonConstructor]
        private Telefone(string numero)
        {
            Numero = numero;
        }

        public static Telefone Criar(string numero)
        {          
            if (string.IsNullOrWhiteSpace(numero))
            {
                return new Telefone("");
            }
            if (Regex.IsMatch(numero, @"^\d{10}$"))
            {
            return new Telefone(numero);
            }
            return null;
        }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(Numero))
            {
                return Numero;
            }
            var sb = new StringBuilder();
            sb.Append("(");
            sb.Append(Numero[0]);
            sb.Append(Numero[1]);
            sb.Append(")");
            sb.Append(" ");
            sb.Append(Numero[2]);
            sb.Append(Numero[3]);
            sb.Append(Numero[4]);
            sb.Append(Numero[5]);
            sb.Append("-");
            sb.Append(Numero[6]);
            sb.Append(Numero[7]);
            sb.Append(Numero[8]);
            sb.Append(Numero[9]);
            return sb.ToString();
        }
    }
}