﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgendaDeContatos.Model
{
    public class Contato
    {
        public string Nome { get; set; }
        public string Foto { get; set; }
        public Telefone Telefone { get; set; }
        public Celular Celular { get; set; }
        public Email Email { get; set; }
    }
}
