﻿using Newtonsoft.Json;

namespace AgendaDeContatos.Model
{
    public class Email
    {
        public string Endereco { get; }

        [JsonConstructor]
        private Email(string endereco)
        {
            Endereco = endereco;
        }

        public static Email Criar(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return new Email("");
            }
            if (!email.Contains("@")) return null;
            if (email.StartsWith("@")) return null;
            if (email.EndsWith("@")) return null;
            return new Email(email);
        }

        public override string ToString()
        {
            return Endereco;
        }
    }
}